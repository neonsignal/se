/* SE structured editor - Copyright (C) Glenn McIntosh 1993-2001 */
/* licensed under the GNU General Public License version 3 */
/* include files */
	#pragma inline
	#include <alloc.h>
	#include <io.h>
	#include <fcntl.h>
	#include <sys\stat.h>
	#include <mem.h>
	#include <string.h>
/* standard types */
	/* primitive constants */
		#define NULL 0L
		#define FALSE 0
		#define TRUE 1
		#define UNDEF -1
		#define MAXUINT 0xFFFF
	/* primitive types */
		typedef char bool;
		typedef unsigned char uchar;
		typedef unsigned int uint;
		typedef unsigned long ulong;
	/* ascii and keyboard constants */
		enum
			{
			NUL = 0x00, SOH, STX, ETX, EOT, ENQ, ACK, BEL, BS, HT, LF, VT, FF, CR, SO,
			SI, DLE, DC1, DC2, DC3, DC4, NAK, SYN, ETB, CAN, EM, SUB, ESC, FS, GS, RS,
			US, SP = 0x20, DEL = 0x7F,
			};
		enum
			{
			C_AT = 0x00, C_A, C_B, C_C, C_D, C_E, C_F, C_G, C_H, C_I, C_J, C_K, C_L,
			C_M, C_N, C_O, C_P, C_Q, C_R, C_S, C_T, C_U, C_V, C_W, C_X, C_Y, C_Z,
			C_OPSQ, C_BKSL, C_CLSQ, C_CT, C_UNSC,
			C_NUL = 0x103,
			S_TAB = 0x10F,
			A_Q = 0x110, A_W, A_E, A_R, A_T, A_Y, A_U, A_I, A_O, A_P,
			A_A = 0x11E, A_S, A_D, A_F, A_G, A_H, A_J, A_K, A_L,
			A_Z = 0x12C, A_X, A_C, A_V, A_B, A_N, A_M,
			F1 = 0x13B, F2, F3, F4, F5, F6, F7, F8, F9, F10,
			HOME = 0x147, UP, PGUP,
			LEFT = 0x14B, RIGHT = 0x14D,
			END = 0x14F, DOWN, PGDN,
			INSERT, DELETE,
			S_F1, S_F2, S_F3, S_F4, S_F5, S_F6, S_F7, S_F8, S_F9, S_F10,
			C_F1, C_F2, C_F3, C_F4, C_F5, C_F6, C_F7, C_F8, C_F9, C_F10,
			A_F1, A_F2, A_F3, A_F4, A_F5, A_F6, A_F7, A_F8, A_F9, A_F10,
			C_PRTSC,
			C_LEFT, C_RIGHT,
			C_END, C_PGDN,
			C_HOME,
			A_1, A_2, A_3, A_4, A_5, A_6, A_7, A_8, A_9, A_0,
			A_MINUS, A_EQUAL,
			C_PGUP,
			};
	/* assembly command */
		#define A asm
/* screen display routines */
	/* include files */
		#include <bios.h>
		#include <conio.h>
	/* definitions */
		/* window size */
			uchar xsize = 80, ysize = 25-1;
		/* colours */
			#define colour(fgcolour, bgcolour) ((bgcolour)<<4|(fgcolour))
		/* special characters */
			enum {VBAR = 0xB3, LEND0 = 0xFE, LEND1 = 0xFA};
	/* variables */
		static uchar attrib = colour(WHITE, BLACK);
	/* routines */
	uint schkc(uint service)
	{
		uint c;
	/* check key */
		if (((c = bioskey(service)) & 0xFF) == 0x00 && c != NUL) c = c>>8 | 0x100;
		else c &= 0xFF;
	/* return key */
		return c;
	}
	static void sinit(void)
	{
	/* calculate x width */
		_AH = 0x0F;
		A int 0x10;
		xsize = _AH;
	/* calculate y height */
		_DH = 0;
		do
		{
			ysize = _DH;
			_AH = 0x0E; _BH = 0x00;
			_AL = 0x0A;
			A int 0x10;
			_AH = 0x03; _BH = 0x00;
			A int 0x10;
		}
		while (_DH != ysize);
	}
	static void scursor(uint y, uint x)
	{
		_AH = 0x02; _BH = 0x00;
		_DL = x; _DH = y;
		A int 0x10;
	}
	static void sinvert(void)
	{
		_AH = 0x08; _BH = 0x00;
		A int 0x10;
		A rol ah,1; A rol ah,1; A rol ah,1; A rol ah,1; A mov bl,ah;
		_AH = 0x09; _BH = 0x00;
		_CX = 1;
		A int 0x10;
	}
	static uint sxcursor(void)
	{
		_AH = 0x03; _BH = 0x00;
		A int 0x10;
		return _DL;
	}
	static void scolour(uchar colour)
	{
		attrib = colour;
	}
	static void sputc(int c)
	{
		_AL = c; _BL = attrib;
		_AH = 0x09; _BH = 0x00;
		_CX = 1;
		A int 0x10;
		_AH = 0x03; _BH = 0x00;
		A int 0x10;
		A inc dl;
		_AH = 0x02; _BH = 0x00;
		A int 0x10;
	}
	static void sputs(char *s)
	{
		while (*s)
			sputc(*s++);
	}
	static void sputs0(char *s)
	{
		for (;*s; ++s)
		{
			_AL = *s;
			_AH = 0x0E; _BH = 0x00;
			A int 0x10;
		}
	}
	static void scleareol(void)
	{
		uint chr;
		chr = xsize - sxcursor();
		_AH = 0x09; _BH = 0x00;
		_AL = SP; _BL = attrib;
		_CX = chr;
		A int 0x10;
	}
/* memory structure */
	/* limits */
		#define MAXCHARS 3071
		#define MAXFNCHARS 1023U
		#define MAXBINCHARS 32U
		#define OPEN 0U
		#define CLOSED 128U
		#define FNAMELEN 64U
	/* line structure */
		typedef struct mline
		{
			uint mlen;
			uchar level;
			struct mline *mprev, *mnext;
			char pChr[0];
		} mline;
	/* base line, paste line */
		static mline baseMline = {0U, 0U+OPEN, &baseMline, &baseMline}, *pBaseMline = &baseMline;
		static mline pasteMline = {0U, 0U+OPEN, &pasteMline, &pasteMline}, *pPasteMline = &pasteMline;
	/* display offsets */
		static uint nline = 0U;
		static uint dline = 0U;
		static mline *pDisplayMline = &baseMline;
		static uint dchr = 0U;
	/* cursor offsets */
		static uint cline = 0U, cursorMline = 0U;
		static mline *pCursorMline = &baseMline;
		static uint cchr = 0U, chchr = 0U, cdchr = 0U, cmchr = 0U;
/* editor status */
	static struct
	{
		uint tabsize;
		bool binMode;
		bool saveMode;
		bool insMode;
		bool detab;
		uchar coltab, colchr;
		ulong end;
	} opts = {2, FALSE, TRUE, TRUE, FALSE, colour(BLUE, LIGHTGRAY), colour(BLACK, LIGHTGRAY)};
	static uint detabsize = 1;
	static int cmdrpt = UNDEF;
	static bool confirmMode = FALSE;
	static bool replaceMode = FALSE;
	static char *errstr = NULL;
	static char *edittitle;
	static char editstr[MAXCHARS+1];
	static int ieditstr = UNDEF;
	static char replacestr[MAXCHARS];
	static int ireplacestr = 0U;
/* function keys */
	static int *fnkey[8U];
	static int fnkey0 = UNDEF, *fnkeyp = &fnkey0;
	static int fnbuf[MAXFNCHARS+1];
	static int fnsave = UNDEF;
	static uint fnlen;
/* memory routines */
	/* character manipulation */
		static bool mword(mline *pMline, uint mchr)
		{
			if (mchr >= pMline->mlen)
				return UNDEF;
			switch (pMline->pChr[mchr])
			{
			case SP: case HT:
				return FALSE;
			default:
				return TRUE;
			}
		}
	/* pointer manipulation */
		static void m0change(mline *pMline, mline *pImline)
		{
			(void) pMline;
			pImline->mprev->mnext = pImline->mnext->mprev = pImline;
		}
		static mline *m0insert(mline *pMline, mline *pImline)
		{
			pImline->mnext = pMline; pImline->mprev = pMline->mprev;
			pImline->mprev->mnext = pMline->mprev = pImline;
			return pImline;
		}
		static mline *m0delete(mline *pMline, mline *pImline)
		{
			pImline->mprev->mnext = pMline; pMline->mprev = pImline->mprev;
			return pImline;
		}
		static void mcheck(mline *pMline, mline *pImline)
		{
			if (pDisplayMline == pMline) pDisplayMline = pImline;
			if (pCursorMline == pMline) pCursorMline = pImline;
		}
		static void mchange(mline *pMline, mline *pImline)
		{
			m0change(pMline, pImline);
			mcheck(pMline, pImline);
		}
		static mline *minsert(mline *pMline, mline *pImline)
		{
			m0insert(pMline, pImline);
			mcheck(pMline, pImline);
			return pImline;
		}
		static mline *mdelete(mline *pMline, mline *pImline)
		{
			m0delete(pMline, pImline);
			mcheck(pImline, pMline);
			return pImline;
		}
		static mline *mnext(mline *pMline)
		{
			while ((pMline = pMline->mnext)->level >= CLOSED);
			return pMline;
		}
		static mline *mprev(mline *pMline)
		{
			while ((pMline = pMline->mprev)->level >= CLOSED);
			return pMline;
		}
	/* line manipulation */
		static void mlineChange(mline *pMline, uint mlen)
		{
			pMline->mlen = mlen;
			mchange(pMline, (mline *) realloc(pMline, mlen+sizeof(mline)));
		}
		static void mlineInsert(mline *pMline, uint mlen, uint level, char *pChr)
		{
			mline *pImline;
			pImline = minsert(pMline, (mline *) malloc(mlen+sizeof(mline)));
			pImline->mlen = mlen;
			pImline->level = level+OPEN;
			memmove(pImline->pChr, pChr, mlen);
			nline += OPEN < CLOSED;
		}
		static void mlineDelete(mline *pMline)
		{
			nline -= pMline->level < CLOSED;
			free(mdelete(pMline->mnext, pMline));
		}
		static void mlinePaste(mline *pMline)
		{
			nline += pPasteMline->mprev->level < CLOSED;
			minsert(pMline, m0delete(pPasteMline, pPasteMline->mprev));
		}
		static void mlineCut(mline *pMline)
		{
			nline -= pMline->level < CLOSED;
			m0insert(pPasteMline, mdelete(pMline->mnext, pMline));
		}
		static void mlineCcut(mline *pMline)
		{
			mline *pImline;
			pImline = m0insert(pPasteMline, (mline *) malloc(pMline->mlen+sizeof(mline)));
			pImline->mlen = pMline->mlen;
			pImline->level = pMline->level;
			memmove(pImline->pChr, pMline->pChr, pMline->mlen);
		}
/* display routines */
	/* variables */
		/* status/display state */
			static bool schange = TRUE;
			enum {CHANGE_CURSX = 0x1, CHANGE_CURSY = 0x2, CHANGE_LINE = 0x4, CHANGE_SCREEN = 0x8};
			static int dchange = CHANGE_SCREEN;
	/* display update */
		static void dstatusint(char *s, uint i)
		{
			uint p, d;
			scolour(opts.coltab);
			sputs(s);
			scolour(opts.colchr);
			for (p = 10000U; p > 0U; p /= 10U)
				sputc((d=i/p)==0 && p>1U ? ' ' : d%10+'0');
			sputc(SP);
		}
		static void dupdateStatus(void)
		{
			uint chr;
			scursor(ysize, 22U);
			scolour(opts.coltab);
			sputs(opts.insMode ? "Ins " : "Ovr ");
			if (fnsave >= 0) dstatusint("FN:", fnlen);
			if (cmdrpt >= 0) dstatusint("Rpt:", cmdrpt);
			if (ieditstr >= 0)
			{
				scolour(opts.coltab);
				sputs(edittitle);
				scolour(opts.colchr);
				chr = 0U; if (xsize-sxcursor() < ieditstr) chr = ieditstr-(xsize-sxcursor());
				while (chr < ieditstr) {sputc(editstr[chr]); ++chr;}
			}
			scleareol();
		}
		static void dupdateLine(uint line, mline *pMline)
		{
			uint chr = dchr, chr0, dlen = dchr+xsize;
			uint level = pMline->level;
			scursor(line, 0U);
			scolour(opts.coltab);
			chr0 = level; if (dlen < chr0) chr0 = dlen;
			while (chr < chr0)
				{sputc(chr%opts.tabsize ? SP : VBAR); ++chr;}
			scolour(opts.colchr);
			chr0 = level+pMline->mlen; if (dlen < chr0) chr0 = dlen;
			while (chr < chr0)
				{sputc(pMline->pChr[chr-level]); ++chr;}
			scolour(opts.coltab);
			if (chr < dlen && chr == chr0 && pMline != pBaseMline)
				{sputc(pMline->mnext->level < CLOSED ? LEND1 : LEND0); ++chr;}
			scleareol();
		}
		static void dupdate(void)
		{
			static uint occhr = 0U;
			static uint ocline = 0U;
			uint line;
			mline *pMline;
			if (schange)
			{
				dupdateStatus();
				scursor(ocline, occhr);
			}
			if (dchange & (CHANGE_CURSX | CHANGE_CURSY))
			{
				scursor(ocline, occhr);
				sinvert();
			}
			if (dchange & CHANGE_SCREEN)
			{
				pMline = pDisplayMline;
				for (line = 0U; line < ysize; ++line)
				{
					dupdateLine(line, pMline);
					if (pMline != pBaseMline) pMline = mnext(pMline);
				}
			}
			else if (dchange & CHANGE_LINE)
				dupdateLine(cline-dline, pCursorMline);
			if (dchange)
			{
				scursor(ysize, 0U);
				if (errstr)
				{
					scolour(colour(WHITE, RED));
					sputs(errstr);
				}
				else
				{
					dstatusint("Line:", cursorMline); dstatusint("Curs:", cdchr);
				}
				scursor(ocline = cline-dline, occhr = cdchr-dchr);
				sinvert();
			}
			schange = dchange = FALSE;
		}
		static void dinit(void)
		{
			scolour(opts.coltab);
			sinit();
		}
		static void ddeinit(void)
		{
			scursor(ysize, 0);
			scolour(colour(WHITE, BLACK));
			sputc('L');
		}
/* editor routines */
	/* key handling */
		/* definitions */
			#define C_Q_ (C_Q<<12)+
			#define C_K_ (C_K<<12)+
		static int getkey(void)
		{
			int key;
		/* wait for input */
			do
			{
				uint i;
			/* process function key */
				if (*fnkeyp >= 0)
					key = *(fnkeyp++);
			/* else wait for key */
				else
				{
				/* update screen */
					if (schkc(1) == NUL && (schange || dchange))
						dupdate();
				/* get key */
					key = schkc(0);
				/* nul translation */
					if (key == C_NUL) key = NUL;
				/* function macro expand */
					if (F1 <= key && key <= F8)
					{
						fnkeyp = fnkey[key-F1];
						continue;
					}
				/* function macro enter */
					if (C_F1 <= key && key <= C_F8)
					{
						if (fnsave < 0)
						{
							fnlen = 0;
							fnsave = key-C_F1;
						}
						else
						{
							fnbuf[fnlen++] = UNDEF;
							free(fnkey[fnsave]);
							memcpy(fnkey[fnsave] = (int *) malloc(fnlen*sizeof(int)), fnbuf, fnlen*sizeof(int));
							fnkeyp = &fnkey0;
							fnsave = UNDEF;
						}
						schange = TRUE;
						continue;
					}
					if (key == C_U)
						if (fnsave >= 0)
						{
							fnsave = UNDEF;
							schange = TRUE;
							continue;
						}
				}
			/* save key to function macro */
				if (fnsave >= 0 && fnlen < MAXFNCHARS)
				{
					fnbuf[fnlen++] = key;
					schange = TRUE;
				}
			/* return key */
				return key;
			}
			while (TRUE);
		}
		static int getstr(char *s)
		{
			int key;
		/* initialize title */
			edittitle = s;
			ieditstr = 0;
			schange = TRUE;
		/* enter string */
			do switch(key = getkey())
			{
			case C_H:
				if (ieditstr)
					{
					--ieditstr;
					schange = TRUE;
					}
				break;
			case ESC: case C_Y:
				ieditstr = UNDEF;
				schange = TRUE;
				break;
			case CR:
				key = ESC;
				break;
			default:
				if (0x20 <= key && key < 0x100 || key == C_P && (key = getkey()) < 0x100)
					{
					editstr[ieditstr++] = key;
					schange = TRUE;
					}
			}
			while (key != ESC);
			return ieditstr;
		}
	/* error routines */
		static int eshow(char *msg)
		{
			int key;
			errstr = msg;
			dchange |= CHANGE_CURSX;
			key = getkey();
			errstr = NULL;
			dchange |= CHANGE_CURSX;
			return key;
		}
	/* cursor commands */
		static void cursSet(uint cchr1, uint *pCdchr1)
			{
			uint clevel = pCursorMline->level;
			cchr = cchr1;
				if (cchr < clevel) cchr = clevel;
				if (cchr > clevel+pCursorMline->mlen) cchr = clevel+pCursorMline->mlen;
				cmchr = cchr-clevel;
			if (cdchr != *pCdchr1)
			{
				cdchr = *pCdchr1;
				dchange |= CHANGE_CURSX;
			}
			if (cdchr < dchr)
			{
				dchr = cdchr;
				dchange |= CHANGE_SCREEN;
			}
			if (cdchr > dchr+(xsize-1U))
			{
				dchr = cdchr-(xsize-1U);
				dchange |= CHANGE_SCREEN;
			}
		}
		static void cursXh(void)
		{
			cursSet(chchr, &cchr);
		}
		static void cursX(uint cchr1)
		{
			cursSet(chchr = cchr1, &cchr);
		}
		static void cursHome(void)
		{
			if (dchr > 0U)
			{
				dchr = 0U;
				dchange |= CHANGE_SCREEN;
			}
			cursSet(chchr = 0U, &cchr);
		}
		static void cursEnd(void)
		{
			cursSet(chchr = MAXUINT, &cchr);
		}
		static void decCline(uint inc)
		{
			cline -= inc;
			while (inc--) do --cursorMline; while ((pCursorMline = pCursorMline->mprev)->level >= CLOSED);
			dchange |= dchange & CHANGE_LINE ? CHANGE_SCREEN : CHANGE_CURSY;
		}
		static void decDline(uint inc)
		{
			dline -= inc;
			while (inc--) pDisplayMline = mprev(pDisplayMline);
			dchange |= CHANGE_SCREEN;
		}
		static void incCline(uint inc)
		{
			cline += inc;
			while (inc--) do ++cursorMline; while ((pCursorMline = pCursorMline->mnext)->level >= CLOSED);
			dchange |= dchange & CHANGE_LINE ? CHANGE_SCREEN : CHANGE_CURSY;
		}
		static void incDline(uint inc)
		{
			dline += inc;
			while (inc--) pDisplayMline = mnext(pDisplayMline);
			dchange |= CHANGE_SCREEN;
		}
		static void cursUp(uint cinc)
		{
			if (cline < 0U+cinc) cinc = cline-0U;
			if (cinc > 0U)
			{
				decCline(cinc);
				if (cline < dline)
				{
					if (dline >= 0U+cinc)
						decDline(cinc);
					else
						{dline = 0U; pDisplayMline = pBaseMline->mnext; incDline(0U);}
				}
			}
		}
		static void cursDn(uint cinc)
		{
			if (cline+cinc >= nline) cinc = nline-cline;
			if (cinc > 0U)
			{
				incCline(cinc);
				if (cline > dline+(ysize-1U))
				{
					if (dline+cinc+(ysize-1U) <= nline)
						incDline(cinc);
					else
						{dline = nline; pDisplayMline = pBaseMline; decDline(ysize-1U);}
				}
			}
		}
		static void scrnUp(uint cinc)
		{
			if (dline >= 0U+cinc)
			{
				decDline(cinc);
				if (cline > dline+(ysize-1U))
				{
					decCline(cinc);
					cursXh();
				}
			}
		}
		static void scrnDn(uint cinc)
		{
			if (dline+cinc <= nline)
			{
				incDline(cinc);
				if (cline < dline)
				{
					incCline(cinc);
					cursXh();
				}
			}
		}
		static void cursLt(void)
		{
			if (cmchr == 0U)
			{
				if (cline > 0U)
					{cursUp(1U); cursX(pCursorMline->level+pCursorMline->mlen);}
			}
			else
				cursX(cchr-1U);
		}
		static void cursRt(void)
		{
			if (cmchr == pCursorMline->mlen)
			{
				if (cline < nline)
					{cursDn(1U); cursX(pCursorMline->level);}
			}
			else
				cursX(cchr+1U);
		}
		static void cursWlt(void)
		{
			if (cmchr == 0U) cursLt();
			while (mword(pCursorMline, cmchr-1U) == FALSE) cursLt();
			while (mword(pCursorMline, cmchr-1U) == TRUE) cursLt();
		}
		static void cursWrt(void)
		{
			while (mword(pCursorMline, cmchr) == TRUE) cursRt();
			while (mword(pCursorMline, cmchr) == FALSE) cursRt();
			if (cmchr == pCursorMline->mlen) cursRt();
		}
	/* text commands */
		static void insert(char cc)
		{
				uint cmlen = pCursorMline->mlen;
			if (cline == nline)
				mlineInsert(pCursorMline, 0U, ++cchr, NULL);
			if (opts.insMode || cmchr == cmlen)
			{
				mlineChange(pCursorMline, cmlen+1U);
				memmove(pCursorMline->pChr+(cmchr+1U), pCursorMline->pChr+cmchr, cmlen-cmchr);
			}
			pCursorMline->pChr[cmchr] = cc;
			dchange |= CHANGE_LINE;
			cursX(cchr+1U);
		}
		static void delete(void)
		{
				mline *pMline;
				uint cmlen = pCursorMline->mlen, mlen;
			if (cline != nline)
			{
				if (cmchr < cmlen)
				{
					memmove(pCursorMline->pChr+cmchr, pCursorMline->pChr+(cmchr+1U), cmlen-(cmchr+1U));
					mlineChange(pCursorMline, cmlen-1U);
					dchange |= CHANGE_LINE;
				}
				else
				{
					pMline = mnext(pCursorMline);
					if (pMline != pBaseMline)
					{
						mlen = pMline->mlen;
						mlineChange(pCursorMline, cmchr+mlen);
						memmove(pCursorMline->pChr+cmchr, pMline->pChr, mlen);
						mlineDelete(pMline);
					}
					else
						if (pMline == pCursorMline->mnext && cmlen == 0U && pCursorMline->level == 0U)
							mlineDelete(pCursorMline);
					dchange |= CHANGE_SCREEN;
				}
			}
		}
		static void deleteW(void)
		{
			while (mword(pCursorMline, cmchr) == TRUE) delete();
			while (mword(pCursorMline, cmchr) == FALSE) delete();
			if (cmchr == pCursorMline->mlen) delete();
		}
		static void deleteE(void)
		{
			mlineChange(pCursorMline, cmchr);
			dchange |= CHANGE_LINE;
		}
		static void insertLn(void)
		{
				mline *pMline;
				uint cmlen = pCursorMline->mlen;
				uint clevel = pCursorMline->level;
			if (cline < nline)
			{
				pMline = pCursorMline->mnext;
				if (cmchr == cmlen) pMline = mnext(pCursorMline);
				mlineInsert(pMline, cmlen-cmchr, clevel, pCursorMline->pChr+cmchr);
				mlineChange(pCursorMline, cmchr);
			}
			else
				mlineInsert(pCursorMline, 0U, ++cchr, NULL);
			dchange |= CHANGE_SCREEN;
		}
		static void deleteLn(void)
		{
			if (cline < nline)
			{
				mlineDelete(pCursorMline);
				while (pCursorMline->level >= CLOSED) mlineDelete(pCursorMline);
				dchange |= CHANGE_SCREEN;
				cursXh();
			}
		}
	/* format commands */
		static void formatPar(void)
		{
				mline *pMline;
				uint clevel = pCursorMline->level;
				uint cmlen = pCursorMline->mlen, mlen;
				uint mchr;
			if (cline < nline && cmlen > 0U && clevel < xsize)
			{
				cursX(clevel);
				do
				{
					while (cmlen+clevel > xsize)
					{
						mchr = xsize+1U-clevel;
						while (mword(pCursorMline, mchr-1U) == FALSE) --mchr;
						while (mword(pCursorMline, mchr-1U) == TRUE) --mchr;
						if (mchr == 0U) mchr = xsize-clevel;
						pMline = pCursorMline->mnext;
						mlineInsert(pMline, cmlen-mchr, clevel, pCursorMline->pChr+mchr);
						mlineChange(pCursorMline, mchr);
						cursDn(1U); cursX(clevel);
						cmlen -= mchr;
					}
				if (cmlen == 0 || mword(pCursorMline, cmlen-1) || (pMline = pCursorMline->mnext) == pBaseMline || pMline->level != clevel) break;
					mlen = pMline->mlen;
					mlineChange(pCursorMline, cmlen+mlen);
					memmove(pCursorMline->pChr+cmlen, pMline->pChr, mlen);
					mlineDelete(pMline);
					cmlen += mlen;
				}
				while (TRUE);
				dchange |= CHANGE_SCREEN;
			}
			cursDn(1U); cursX(clevel);
		}
		static void formatFile(void)
		{
			while (cline < nline)
				formatPar();
		}
	/* frame commands */
		static void levelIn(void)
		{
				mline *pMline;
				uint level = pCursorMline->level == 0U ? 1U : opts.tabsize;
			if (cline == nline)
				mlineInsert(pCursorMline, 0U, 0U, NULL);
			pCursorMline->level += level;
			pMline = pCursorMline;
			while ((pMline = pMline->mnext)->level >= CLOSED)
				pMline->level += opts.tabsize;
			dchange |= CHANGE_LINE;
			cursX(cchr+level);
		}
		static void levelOut(void)
		{
				mline *pMline;
				uint level = pCursorMline->level == 1U ? 1U : opts.tabsize;
			if (pCursorMline->level >= level)
			{
				pCursorMline->level -= level;
				pMline = pCursorMline;
				while ((pMline = pMline->mnext)->level >= CLOSED)
					pMline->level -= opts.tabsize;
				dchange |= CHANGE_LINE;
				cursX(cchr-level);
			}
		}
		static void frameSwitch(uint open)
		{
			mline *pMline;
			uint clevel = pCursorMline->level, level;
			uint iopen;
			if (cline < nline)
			{
				pMline = pCursorMline->mnext;
				iopen = pMline->level & CLOSED ^ CLOSED;
				while (pMline->level > clevel)
				{
					level = pMline->level;
					nline = nline - (pMline->level < CLOSED) + (iopen < CLOSED);
					pMline->level = pMline->level & CLOSED-1 | iopen;
					for (pMline = pMline->mnext; pMline->level > level; pMline = pMline->mnext)
					{
						nline = nline - (pMline->level < CLOSED) + ((iopen|open) < CLOSED);
						pMline->level = pMline->level & CLOSED-1 | (iopen|open);
					}
				}
				dchange |= CHANGE_SCREEN;
			}
		}
	/* block commands */
		static void pasteLn(void)
		{
			while (pPasteMline->mnext != pPasteMline)
				mlinePaste(pCursorMline);
			dchange |= CHANGE_SCREEN;
			cursXh();
		}
		static void cutLn(void)
		{
			if (cline < nline)
			{
				mlineCut(pCursorMline);
				while (pCursorMline->level >= CLOSED)
					mlineCut(pCursorMline);
				dchange |= CHANGE_SCREEN;
				cursXh();
			}
		}
		static void ccutLn(void)
		{
				mline *pMline;
			if (cline < nline)
			{
				mlineCcut(pCursorMline);
				pMline = pCursorMline;
				while ((pMline = pMline->mnext)->level >= CLOSED)
					mlineCcut(pMline);
			}
		}
	/* file commands */
		static void readFile(char *sfile, uint mlen, mline *pMline)
		{
			int hfile;
			uint len0, len1;
			char fbuf[MAXCHARS];
			uint fbuflen;
			uint indent;
		/* open file */
			mlineInsert(pMline, mlen, 0U, sfile);
			if ((hfile = open(sfile, O_RDONLY | (opts.binMode ? O_BINARY : O_TEXT))) >= 0)
			{
			/* load file */
				if (opts.binMode)
					while (fbuflen = read(hfile, fbuf, MAXBINCHARS))
						mlineInsert(pMline, fbuflen, 1U, fbuf);
				else
					for (len1 = 0U;
						fbuflen = read(hfile, fbuf+len1, sizeof(fbuf)-len1) + len1;
						memmove(fbuf, fbuf+len0, len1 -= len0))
					{
						len0 = 0U;
						do
						{
							while (len1 < fbuflen && fbuf[len1] != LF) ++len1;
							if (len1 < fbuflen || len0 == 0U)
							{
								indent = 0;
								while (len0 < len1 && fbuf[len0] == HT)
								{
									indent += opts.tabsize;
									++len0;
								}
								while (len0 < len1 && fbuf[len0] == SP)
								{
									++indent;
									++len0;
								}
								len0 -= indent%opts.tabsize;
								indent = indent/opts.tabsize*opts.tabsize;
								mlineInsert(pMline, len1-len0, 1U+indent, fbuf+len0);
								if (len1 < fbuflen) ++len1;
								len0 = len1;
							}
						}
						while (len1 < fbuflen);
					}
			/* close file */
				close(hfile);
			}
			dchange |= CHANGE_SCREEN;
		}
		static char *flevel;
		static mline *saveFrame(mline *pMline)
		{
			char *sfile = "default.txt";
			int hfile;
			char fname[FNAMELEN+1];
			uint mlen;
			uint level;
		/* get filename */
			if (pMline->level == 0U)
			{
				mlen = pMline->mlen; if (FNAMELEN < mlen) mlen = FNAMELEN;
				memcpy(fname, pMline->pChr, mlen);
				fname[mlen] = NUL;
				sfile = fname;
				pMline = pMline->mnext;
			}
		/* save file */
			if (opts.saveMode &&
				(hfile = open(sfile, O_WRONLY|(opts.binMode?O_BINARY:O_TEXT)|O_CREAT|O_TRUNC, S_IREAD|S_IWRITE)) >= 0)
			{
			/* write lines */
				if (opts.binMode)
					while (pMline->level > 0U)
					{
						write(hfile, pMline->pChr, pMline->mlen);
						pMline = pMline->mnext;
					}
				else
					while ((level = pMline->level) > 0U)
					{
						write(hfile, flevel, ((level&CLOSED-1)-1U)/detabsize);
						write(hfile, pMline->pChr, pMline->mlen);
						write(hfile, "\n", 1);
						pMline = pMline->mnext;
					}
			/* close file */
				close(hfile);
			}
			else
			{
			/* skip lines */
				while (pMline->level > 0U) pMline = pMline->mnext;
			/* error */
				eshow("filesave failed    ");
			}
			return pMline;
		}
		static void saveFile(void)
		{
				mline *pMline = pCursorMline;
			while (pMline->level > 0U && pMline->mprev != pBaseMline) pMline = pMline->mprev;
			saveFrame(pMline);
		}
		static void saveAll(void)
		{
				mline *pMline = pBaseMline->mnext;
			while (pMline != pBaseMline)
				pMline = saveFrame(pMline);
		}
	/* search commands */
		/* regular expression routines */
			/* code storage */
				static enum
				{
					SEQ, SEQ0, SEQ1, ALT, SEQDONE,
					MATCH, MATCHALL, MATCHBEGIN, MATCHEND,
					SET, SETDONE,
					RANGEMATCH, RANGEBOUND,
					};
				static char regcode[256];
				static char regval[256];
				static int ireg;
				static char *regstr;
				static int iregstr, lregstr;
				static bool regerr;
			/* parser routines */
				static char ichar;
				static bool testchar(char tchar)
				{
					if (ichar != tchar) return FALSE;
					ichar = editstr[++iregstr];
					return TRUE;
				}
				static void setchar(void)
				{
					testchar('\\');
					if (iregstr < ieditstr)
					{
						regval[ireg] = ichar;
						testchar(ichar);
					}
					else regerr = TRUE;
				}
				static void regseq(void)
				{
						uint iregseq, iregalt;
					while (iregstr < ieditstr && ichar != '|' && ichar != ')')
					{
						iregseq = ireg++;
						if (testchar('('))
						{
							do
							{
								iregalt = ireg++;
								regseq();
								regcode[iregalt] = ALT;
								regval[iregalt] = ireg;
							}
							while (testchar('|'));
							regcode[ireg] = SEQDONE;
							regval[ireg] = FALSE;
							if (!testchar(')')) regerr = TRUE;
						}
						else if (testchar('['))
						{
							regcode[ireg] = SET;
							regval[ireg] = !testchar('^');
							while (++ireg, iregstr < ieditstr && ichar != ']')
							{
								setchar();
								if (testchar('-'))
								{
									regcode[ireg++] = RANGEBOUND;
									setchar();
									regcode[ireg] = RANGEBOUND;
								}
								else
									regcode[ireg] = RANGEMATCH;
							}
							regcode[ireg] = SETDONE;
							if (!testchar(']')) regerr = TRUE;
						}
						else
						{
							regcode[ireg] =
								ichar == '.' ? MATCHALL :
								ichar == '^' ? MATCHBEGIN :
								ichar == '$' ? MATCHEND :
								MATCH;
							setchar();
						}
						ireg++;
						regcode[iregseq] = testchar('*') ? SEQ0 : testchar('+') ? SEQ1 : SEQ;
						regval[iregseq] = ireg;
					}
					regcode[ireg] = SEQDONE;
					regval[ireg++] = TRUE;
				}
			/* executor routines */
				static bool regstate(int ireg)
				{
					while (TRUE) switch (regcode[ireg])
					{
					case SEQ:
						if (! (_AX = regstate(ireg+1))) return _AX;
						ireg = regval[ireg];
						break;
					case SEQ0:
					{
						register int iregstr0;
						iregstr0 = iregstr;
						if (regstate(ireg+1))
							if (_AX = regstate(ireg)) return _AX;
						iregstr = iregstr0;
					}
						ireg = regval[ireg];
						break;
					case SEQ1:
						if (!(_AX = regstate(ireg+1))) return _AX;
					{
							register int iregstr0;
						iregstr0 = iregstr;
						if (_AX = regstate(ireg)) return _AX;
						iregstr = iregstr0;
					}
						ireg = regval[ireg];
						break;
					case ALT:
					{
						register int iregstr0;
						iregstr0 = iregstr;
						if (_AX = regstate(ireg+1)) return _AX;
						iregstr = iregstr0;
					}
						ireg = regval[ireg];
						break;
					case SEQDONE:
						return regval[ireg];
					case MATCH:
						return iregstr < lregstr && regval[ireg] == regstr[iregstr++];
					case MATCHALL:
						return iregstr++ < lregstr;
					case MATCHBEGIN:
						return iregstr == 0;
					case MATCHEND:
						return iregstr == lregstr;
					case SET:
						if (iregstr == lregstr) return FALSE;
					{
						register int val = regval[ireg];
						while (regcode[++ireg] != SETDONE)
							if (regcode[ireg] == RANGEBOUND &&
								regval[ireg++] <= regstr[iregstr] && regstr[iregstr] <= regval[ireg] ||
								regval[ireg] == regstr[iregstr])
								{iregstr += val; return val;}
						iregstr += !val; return !val;
					}
					}
				}
			static void reginit(void)
			{
				regcode[0] = SEQDONE;
				regval[0] = TRUE;
			}
			static int regparse(void)
			{
				regerr = FALSE;
				ireg = 0;
				ichar = editstr[iregstr = 0U];
				regseq();
				if (iregstr < ieditstr) regerr = TRUE;
				if (regerr) reginit();
				return regerr;
			}
			static int regsearch(void)
			{
					uint l1;
				regstr = pCursorMline->pChr;
				lregstr = pCursorMline->mlen;
				for (l1 = cmchr; (iregstr = l1) <= lregstr; ++l1)
					if (regstate(0)) return l1;
				return l1;
			}
			static int regsearch1(void)
			{
				return iregstr;
			}
		static bool srchInit(void)
		{
			if (regparse())
			{
				eshow("search syntax error");
				return FALSE;
			}
			return TRUE;
		}
		static bool replaceInit(void)
		{
			memcpy(replacestr, editstr, ieditstr);
			ireplacestr = ieditstr;
			return TRUE;
		}
		static int srchNext(void)
		{
			while (cline < nline && (cmchr = regsearch()) > pCursorMline->mlen)
			{
				incCline(1U);
				cmchr = 0U;
			}
			if (cline == nline) cmchr = 0U;
			if (cline > dline+(ysize-1U))
				incDline(cline-(ysize-1U) - dline);
			cursX(cmchr+pCursorMline->level);
			return cline != nline;
		}
		static int srchReplace(void)
		{
				int key;
				uint cmlen = pCursorMline->mlen, mlen;
				uint cmchr1;
			mlen = (cmchr1 = regsearch1()) - cmchr;
			if ((!confirmMode || (key = eshow("SP=skip,RET=replace")) == CR) && replaceMode)
			{
				mlen = ireplacestr;
				if (mlen < cmchr1-cmchr)
				{
					memmove(pCursorMline->pChr+cmchr+mlen, pCursorMline->pChr+cmchr1, cmlen-cmchr1);
					mlineChange(pCursorMline, cmlen-(cmchr1-cmchr)+mlen);
				}
				else
				{
					mlineChange(pCursorMline, cmlen-(cmchr1-cmchr)+mlen);
					memmove(pCursorMline->pChr+cmchr+mlen, pCursorMline->pChr+cmchr1, cmlen-cmchr1);
				}
				memcpy(pCursorMline->pChr+cmchr, replacestr, mlen);
				dchange |= CHANGE_LINE;
			}
			if (!confirmMode || key != ESC)
				cursX(chchr = cchr+mlen);
			return confirmMode && key != ESC;
		}
	/* sort commands */
		static mline *pSmline;
		static int scmp(char *pChr0, uint mlen0, char *pChr1, uint mlen1)
		{
				uint mchr;
			if (mlen0 > mlen1) mlen0 = mlen1;
			for (mchr = 0; mchr < mlen0; ++mchr)
				if (pChr0[mchr] != pChr1[mchr])
					return pChr0[mchr] < pChr1[mchr];
			return mlen0 < mlen1;
		}
		static void smerge(uint n0, uint n1, mline *pMline0, mline *pMline1)
		{
			while (TRUE) /* assume pMline0 follows pMline1 */
			{
				if (n0 && (!n1 || scmp(pMline0->pChr, pMline0->mlen, pMline1->pChr, pMline1->mlen)))
				{
					pMline0 = mnext(pMline0);
					--n0;
				}
				else if (n1)
				{
					do
						m0insert(pMline0, m0delete(pMline1 = pMline1->mnext, pMline1));
					while (pMline1->level >= CLOSED);
					--n1;
				}
				else
					return;
			}
		}
		static mline *srecurse(uint n)
		{
				mline **ppMline = &pSmline->mprev->mnext;
			if (n > 1)
				smerge((n)/2, (n+1)/2, srecurse((n)/2), srecurse((n+1)/2));
			else
				pSmline = mnext(pSmline);
			return *ppMline;
		}
		static void sort(void)
		{
				uint n = 0;
				uint clevel = pCursorMline->level;
				mline *pMline;
			if (pCursorMline != pBaseMline)
			{
				pSmline = pCursorMline->mnext;
				for (pMline = pSmline; pMline->level > clevel; pMline = pMline->mnext)
					n += pMline->level < CLOSED;
			}
			if (n > 0)
			{
				srecurse(n);
				dchange |= CHANGE_SCREEN;
			}
		}
	static void ed(void)
	{
		int key, key1;
		uint fninit;
		uint level;
		enum {NORMAL, REVERSE};
		static bool direction = NORMAL;
	/* initialize function keys */
		for (fninit = 0; fninit < sizeof(fnkey)/sizeof(fnkey[0]); ++fninit)
			(fnkey[fninit] = (int *) malloc(1*sizeof(int)))[0] = UNDEF;
	/* initialize save level buffer */
		if (!opts.detab) detabsize = opts.tabsize;
		flevel = (char *) malloc(127U/detabsize);
		for (level = 0U; level < 127U/detabsize; ++level)
			flevel[level] = opts.detab ? SP : HT;
	/* initialize search routines */
		reginit();
	/* process commands */
		while (TRUE)
		{
		/* get command */
			key = getkey();
		/* repetition command */
			while (key == C_CT)
			{
				cmdrpt = 0;
				schange = TRUE;
				do switch (key1 = getkey())
				{
				case '1': case '2': case '3': case '4': case '5':
				case '6': case '7': case '8': case '9': case '0':
					if (cmdrpt < 1000)
					{
						cmdrpt = cmdrpt*10+(key1-'0');
						schange = TRUE;
					}
					break;
				case C_H:
					cmdrpt /= 10;
					schange = TRUE;
					break;
				case ESC:
					cmdrpt = 0;
					key = key1;
					schange = TRUE;
					break;
				default:
					key = key1;
					key1 = ESC;
				}
				while (key1 != ESC);
			}
		/* second key commands */
			if (key == C_Q || key == C_K)
			{
				key <<= 12;
				key1 = getkey();
				if ('A' <= key1 && key1 <= 'Z') key1 += C_A-'A';
				else if ('a' <= key1 && key1 <= 'z') key1 += C_A-'a';
				key += key1;
			}
		/* text entry */
			switch (key)
			{
			case C_K_ C_R:
				if (getstr("Read:") < 0) key = ESC;
				break;
			case C_Q_ C_F: case C_Q_ C_A:
				replaceMode = key == C_Q_ C_A;
				confirmMode = cmdrpt == 0;
				cmdrpt -= confirmMode;
				if (!(getstr("Find:") >= 0 && srchInit() &&
					(!replaceMode || getstr("Replace with:") >= 0 && replaceInit())))
					key = ESC;
				break;
			}
		/* execute command */
			for (;cmdrpt == -1 || cmdrpt > 0; --cmdrpt) switch (key)
			{
			/* cursor commands */
			case UP: case C_E: cursUp(1U); cursXh(); break;
			case DOWN: case C_X: cursDn(1U); cursXh(); break;
			case PGUP: case C_R: cursUp(ysize); cursXh(); break;
			case PGDN: case C_C: cursDn(ysize); cursXh(); break;
			case LEFT: case C_S: cursLt(); break;
			case RIGHT: case C_D: cursRt(); break;
			case C_LEFT: case C_A: cursWlt(); break;
			case C_RIGHT: case C_F: cursWrt(); break;
			case C_W: scrnUp(1U); break;
			case C_Z: scrnDn(1U); break;
			case C_PGUP: case C_Q_ C_R: cursUp(nline); cursXh(); break;
			case C_PGDN: case C_Q_ C_C: cursDn(nline); cursXh(); break;
			case C_HOME: case C_Q_ C_E: cursUp(cline-dline); cursXh(); break;
			case C_END: case C_Q_ C_X: cursDn(dline+(ysize-1U)-cline); cursXh(); break;
			case HOME: case C_Q_ C_S: cursHome(); break;
			case END: case C_Q_ C_D: cursEnd(); break;
			/* text commands */
			/* case ascii_32_to_127: case C_P: see default */
			case C_H: cursLt(); if (opts.insMode) delete(); break;
			case DELETE: case C_G: delete(); break;
			case C_N: insertLn(); break;
			case CR: if (opts.insMode) insertLn(); cursDn(1U); cursHome(); break;
			case C_T: deleteW(); break;
			case C_Y: deleteLn(); break;
			case C_Q_ C_Y: deleteE(); break;
			/* format commands */
			case C_B: formatPar(); break;
			case C_Q_ C_B: formatFile(); break;
			/* frame commands */
			case HT: levelIn(); break;
			case S_TAB: levelOut(); break;
			case LF: frameSwitch(CLOSED); break;
			case C_Q_ LF: frameSwitch(OPEN); break;
			/* block commands */
			case C_K_ C_P: pasteLn(); break;
			case C_K_ C_Y: cutLn(); break;
			case C_K_ C_C: ccutLn(); break;
			/* file commands */
			case C_K_ C_Q: return;
			case C_K_ C_S: saveAll(); break;
			case C_K_ C_X: saveAll(); return;
			case C_K_ C_R: editstr[ieditstr] = NUL; readFile(editstr, ieditstr, pCursorMline); break;
			case C_K_ C_W: saveFile(); break;
			/* search commands */
			case C_L: case C_Q_ C_F: case C_Q_ C_A: while (srchNext() && srchReplace()); break;
			/* sort commands */
			case C_K_ '=': sort(); break;
			/* mode commands */
			case INSERT: case C_V: opts.insMode = !opts.insMode; schange = TRUE; break;
			/* direction commands */
			case A_N: direction = NORMAL; break;
			case A_R: direction = REVERSE; break;
			/* characters */
			default:
				if (0x20 <= key && key < 0x100 || key == C_P && (key = getkey()) < 0x100)
					{
						insert(key);
						if (direction == REVERSE)
							cursLt();
					}
				break;
			}
			if (++cmdrpt > 0) {cmdrpt = -1; schange = TRUE;}
			if (ieditstr >= 0)
			{
				ieditstr = UNDEF;
				schange = TRUE;
			}
		}
	}
int main(int argc, char *argv[])
{
	char **av = argv, *avn;
	int optsave;
	int hopt;
/* get options/arguments */
	optsave = argv[argc-1][0] == '!';
	hopt = open(argv[0], O_RDONLY | O_BINARY);
	opts.end = lseek(hopt, 0l, SEEK_END);
	if (!(optsave && argv[argc-1][1] == '+'))
	{
		lseek(hopt, -(long) sizeof(opts), SEEK_END);
		read(hopt, &opts, sizeof(opts));
	}
	close(hopt);
	while (++av != argv+argc-optsave) switch (*(avn = *av))
	{
	case '-': case '/':
		while (*++avn) switch(*avn)
		{
		/* indent size */
			case 'I': if ('1' <= *(avn+1) && *(avn+1) <= '8')
				opts.tabsize = *++avn - '0'; break;
		/* text/binary mode */
			case 't': opts.binMode = FALSE; break;
			case 'b': opts.binMode = TRUE; break;
		/* edit/view mode */
			case 'e': opts.saveMode = TRUE; break;
			case 'v': opts.saveMode = FALSE; break;
		/* normal/inverse colours */
			case 'G': opts.coltab = colour(BLUE, LIGHTGRAY); opts.colchr = colour(BLACK, LIGHTGRAY); break;
			case 'W': opts.coltab = colour(BLUE, WHITE); opts.colchr = colour(BLACK, WHITE); break;
			case 'B': opts.coltab = colour(BLUE, BLACK); opts.colchr = colour(LIGHTGRAY, BLACK); break;
		/* insert/overtype mode */
			case 'i': opts.insMode = TRUE; break;
			case 'o': opts.insMode = FALSE; break;
		/* entabbed file load */
			case 'E': opts.detab = FALSE; break;
			case 'D': opts.detab = TRUE; break;
		/* help function */
			default:
				sputs0("Usage: "); sputs0(argv[0]);
				sputs0("<V3.0> [-options] [files ...] [!]\r\n"
					"  -?       : help\r\n"
					"  -I2      : indent size\r\n"
					"  -t/-b    : text/binary mode\r\n"
					"  -e/-v    : edit/view mode\r\n"
					"  -G/-W/-B : grey/white/black background\r\n"
					"  -i/-o    : insert/overtype mode\r\n"
					"  -E/-D    : entabbed/detabbed file save\r\n"
					"  files    : files to load\r\n"
					"  !        : set as defaults\r\n"
					);
				return 0;
		}
		break;
	default:
		readFile(avn, strlen(avn), pBaseMline);
	}
	if (optsave)
	{
		hopt = open(argv[0], O_WRONLY | O_BINARY);
		lseek(hopt, opts.end, SEEK_SET);
		write(hopt, &opts, sizeof(opts));
		close(hopt);
		return 0;
	}
/* run editor */
	dinit();
	ed();
	ddeinit();
/* return */
	return 0;
}
