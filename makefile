# Borland makefile
# Borland Turbo C compiler

# se
se.exe: se.c
	-tc -f- -O -Z se.c

se0.exe: se.exe
	mv SE.EXE se0.exe
	start se0 -W !+

clean:
	-rm se.obj
	-rm se.exe
	-rm se0.exe
